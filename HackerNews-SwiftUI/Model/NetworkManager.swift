//
//  NetworkManager.swift
//  HackerNews-SwiftUI
//
//  Created by Samat Murzaliev on 20.04.2022.
//

import Foundation

class NetworkManager: ObservableObject {
    
    @Published var posts = [Post]()
    
    func fetchData() {
        if let url = URL(string: "http://hn.algolia.com/api/v1/search?tags=front_page") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { data, response, error in
                
                if error != nil {
                    print("Error receiving data: \(String(describing: error))")
                } else {
                    let decoder = JSONDecoder()
                    if let safeData = data {
                        do {
                            let restuls = try decoder.decode(NewsModel.self, from: safeData)
                            DispatchQueue.main.async {
                                self.posts = restuls.hits
                            }
                        } catch {
                            print("Found error while decoding data: \(error)")
                        }
                    }
                }
            }
            task.resume()
        }
    }
}
