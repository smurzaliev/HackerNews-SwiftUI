//
//  NewsModel.swift
//  HackerNews-SwiftUI
//
//  Created by Samat Murzaliev on 20.04.2022.
//

import Foundation

struct NewsModel: Decodable {
    let hits: [Post]
}

struct Post: Decodable, Identifiable {
    var id: String {
        return objectID
    }
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
