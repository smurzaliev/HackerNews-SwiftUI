//
//  HackerNews_SwiftUIApp.swift
//  HackerNews-SwiftUI
//
//  Created by Samat Murzaliev on 20.04.2022.
//

import SwiftUI

@main
struct HackerNews_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
